package fr.poly.mtp.ig5.iwa.document.controller;

import ch.qos.logback.core.net.SyslogOutputStream;
import fr.poly.mtp.ig5.iwa.document.entity.DisplayDocument;
import fr.poly.mtp.ig5.iwa.document.entity.Document;
import fr.poly.mtp.ig5.iwa.document.service.DocumentService;
import org.keycloak.TokenVerifier;
import org.keycloak.common.VerificationException;
import org.keycloak.representations.AccessToken;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.security.RolesAllowed;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RolesAllowed("user")
@RequestMapping("/documents")
public class DocumentController {

    private final DocumentService documentService;

    DocumentController(DocumentService documentService) {
        this.documentService = documentService;
    }


    @GetMapping("")
    public List<DisplayDocument> getAllDocuments(@RequestHeader (name="Authorization") String a) throws VerificationException {
        String tokenString = a.split(" ")[1];
        AccessToken token = TokenVerifier.create(tokenString, AccessToken.class).getToken();
        List<Document> docs =  documentService.findByUser(token.getSubject());
        List<DisplayDocument> dispDocs = new ArrayList<>();
        for(int i = 0; i < docs.size(); i++){
            DisplayDocument dispDoc = new DisplayDocument(docs.get(i).getFile_id(), docs.get(i).getDocument_url(), docs.get(i).getType(), docs.get(i).getUser());
            dispDoc.setCreation_date((docs.get(i).getCreation_date().getDate()<10?"0"+docs.get(i).getCreation_date().getDate():docs.get(i).getCreation_date().getDate())+ " - " + (docs.get(i).getCreation_date().getMonth()+1<10?"0"+(docs.get(i).getCreation_date().getMonth()+1):docs.get(i).getCreation_date().getMonth()+1)+ " - "  + (docs.get(i).getCreation_date().getYear()+1900));
            dispDocs.add(dispDoc);
        }

        return dispDocs;
    }

    @GetMapping("{id}")
    public Document getById(@PathVariable Long id){ return documentService.findById(id); }

    @GetMapping("/download/{id}")
    public byte[] getFile(@PathVariable Long id){
        Document doc = documentService.findById(id);
        return doc.getDocument_data();
    }


    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    Document newDocument(@RequestParam("document") MultipartFile file, @RequestParam("type") String type,@RequestParam("date")String date, @RequestParam("user")String user_id) throws IOException {
        Document document = new Document();
        document.setDocument_url(file.getOriginalFilename());
        document.setDocument_data(file.getBytes());
        document.setType(type);
        document.setCreation_date(new Date(Long.parseLong(date)));
        document.setUser(user_id);

        System.out.println("doc id "+ document.getFile_id());
        return documentService.save(document);
    }

    @DeleteMapping("{id}")
    void delete(@PathVariable Long id) { documentService.delete(id);}
}
