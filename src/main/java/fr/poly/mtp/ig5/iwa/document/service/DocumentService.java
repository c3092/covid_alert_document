package fr.poly.mtp.ig5.iwa.document.service;

import fr.poly.mtp.ig5.iwa.document.entity.DisplayDocument;
import fr.poly.mtp.ig5.iwa.document.entity.Document;
import fr.poly.mtp.ig5.iwa.document.exceptions.NoDocumentFoundException;
import fr.poly.mtp.ig5.iwa.document.repository.DocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Service
public class DocumentService {

    @Autowired
    DocumentRepository documentRepository;

    public List<Document> findAll(){ return documentRepository.findAll(); }

    public List<Document> findByUser(String user_id){
        return documentRepository.findByUser(user_id); }

    public Document findById(Long id){ return documentRepository.findById(id).orElseThrow(() -> new NoDocumentFoundException(id)); }

    public Document save(Document document){ return documentRepository.saveAndFlush(document); }

    public void delete(Long id){ documentRepository.deleteById(id); }
}
