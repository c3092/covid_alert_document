package fr.poly.mtp.ig5.iwa.document;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "fr")
public class ApplicationDocument {

    public static void main(String[] args){
        SpringApplication.run(ApplicationDocument.class, args);
    }

}
