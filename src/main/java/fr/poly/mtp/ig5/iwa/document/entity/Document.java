package fr.poly.mtp.ig5.iwa.document.entity;

import javax.persistence.*;
import java.util.Date;

@Table(name = "File")
@Entity(name = "fr/poly/mtp/ig5/iwa/document")
@Access(AccessType.FIELD)
public class Document {

    private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Long file_id;
    private String document_url;
    private byte[] file_data;
    private Date creation_date;
    private String type;
    @Column(name="user_id")
    private String user;

    public Document() {
    }

    public Document(String document_url, byte[] file_data, Date creation_date, String type, String user) {
        this.document_url = document_url;
        this.file_data = file_data;
        this.creation_date = creation_date;
        this.type = type;
        this.user = user;
    }

    public Long getFile_id() {
        return file_id;
    }

    public void setFile_id(Long document_id) {
        this.file_id = document_id;
    }

    public String getDocument_url() {
        return document_url;
    }

    public Date getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(Date creation_date) {
        this.creation_date = creation_date;
    }

    public void setDocument_url(String document) {
        this.document_url = document;
    }

    public byte[] getDocument_data() {
        return file_data;
    }

    public void setDocument_data(byte[] document_data) {
        this.file_data = document_data;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
