package fr.poly.mtp.ig5.iwa.document.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

public class DisplayDocument {

    private Long file_id;
    private String document_url;
    private String creation_date;
    private String type;
    private String user_id;

    public DisplayDocument(Long file_id, String document_url, String type, String user_id) {
        this.file_id = file_id;
        this.document_url = document_url;
        this.creation_date = "";
        this.type = type;
        this.user_id = user_id;
    }

    public Long getFile_id() {
        return file_id;
    }

    public void setFile_id(Long file_id) {
        this.file_id = file_id;
    }

    public String getDocument_url() {
        return document_url;
    }

    public void setDocument_url(String document_url) {
        this.document_url = document_url;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
