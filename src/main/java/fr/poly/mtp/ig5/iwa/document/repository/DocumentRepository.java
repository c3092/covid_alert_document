package fr.poly.mtp.ig5.iwa.document.repository;

import fr.poly.mtp.ig5.iwa.document.entity.DisplayDocument;
import fr.poly.mtp.ig5.iwa.document.entity.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import java.util.List;

public interface DocumentRepository extends JpaRepository<Document, Long> {

    List<Document> findByUser(String user_id);
}
