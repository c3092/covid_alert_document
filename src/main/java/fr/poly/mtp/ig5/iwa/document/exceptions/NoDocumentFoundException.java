package fr.poly.mtp.ig5.iwa.document.exceptions;

public class NoDocumentFoundException extends RuntimeException{

    public NoDocumentFoundException( Long id) { super("No document found for this id : " + id) ;}
}
