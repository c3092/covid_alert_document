package fr.poly.mtp.ig5.iwa.document;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.poly.mtp.ig5.iwa.document.controller.DocumentController;
import fr.poly.mtp.ig5.iwa.document.entity.Document;
import fr.poly.mtp.ig5.iwa.document.service.DocumentService;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.mockito.Mockito.doReturn;


@WebMvcTest
public class DocumentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DocumentService service;

    @Autowired
    private DocumentController controller;

    @Test
    @DisplayName("GET /document - Found")
    public void getDocForUserTestFound() throws Exception {

        byte[] testFileData = {};
        Date testFileDate = new Date();
        Document mockDocument = new Document("testFile.pdf", testFileData, testFileDate, "testType", "testUserId");
        doReturn(Optional.of(mockDocument)).when(service).findByUser("testUserId");

        this.mockMvc.perform(get("/document").header("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ0ZXN0VXNlcklkIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.vpJcdDcVE5r-kTcx0AOiwo_wqfbxVfp23UDnRTgZK1I"))
                .andDo(print())
                .andExpect(status().isOk())

                .andExpect(jsonPath("$.document_url", is("testFile.pdf")))
                .andExpect(jsonPath("$.file_data", is(testFileData)))
                .andExpect(jsonPath("$.creation_date", is(testFileDate)))
                .andExpect(jsonPath("$.type", is("testType")))
                .andExpect(jsonPath("$.user", is("testUserId")));


    }

    @Test
    @DisplayName("GET /document - Not Found")
    public void getDocForUserTestNotFound() throws Exception{
        byte[] testFileData = {};
        Date testFileDate = new Date();
        Document mockDocument = new Document("testFile.pdf", testFileData, testFileDate, "testType", "testNotFound");
        doReturn(Optional.empty()).when(service).findByUser("testNotFound");

        this.mockMvc.perform(get("/document").header("Authorization","eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ0ZXN0Tm90Rm91bmQiLCJuYW1lIjoiZGZkIERvZSIsImlhdCI6MTUxNjIzOTAyMn0.D3MPSLfjvjswqY6eozQGRe_qNJjeF-DFuPm1O7GcG9I"))
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("GET /document/1 - Found")
    public void getDocumentByIdTestFound() throws Exception{
        byte[] testFileData = {};
        Date testFileDate = new Date();
        Document mockDocument = new Document("testFile.pdf", testFileData, testFileDate, "testType", "testUserId");

        mockDocument.setFile_id(Long.valueOf(1));

        doReturn(Optional.of(mockDocument)).when(service).findById(Long.valueOf(1));

        this.mockMvc.perform(get("/document/{id}", Long.valueOf(1)))
                .andExpect(status().isOk())

                .andExpect(jsonPath("$.document_url", is("testFile.pdf")))
                .andExpect(jsonPath("$.file_data", is(testFileData)))
                .andExpect(jsonPath("$.creation_date", is(testFileDate)))
                .andExpect(jsonPath("$.type", is("testType")))
                .andExpect(jsonPath("$.user", is("testUserId")));
    }

    @Test
    @DisplayName("GET /document/1 - Not Found")
    public void getDocumentByIdTestNotFound() throws Exception {
        byte[] testFileData = {};
        Date testFileDate = new Date();
        Document mockDocument = new Document("testFile.pdf", testFileData, testFileDate, "testType", "testUserId");

        mockDocument.setFile_id(Long.valueOf(1));

        doReturn(Optional.empty()).when(service).findById(Long.valueOf(1));

        this.mockMvc.perform(get("/document/{id}", Long.valueOf(1)))
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("POST /document - Success")
    public void createDocumentTestSuccess() throws Exception{
        byte[] testFileData = {};
        Date testFileDate = new Date();
        Document mockDocument = new Document("testFile.pdf", testFileData, testFileDate, "testType", "testUserId");
        doReturn(mockDocument).when(service).save(any());

        this.mockMvc.perform(post("/document")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(mockDocument)))

                .andExpect(status().isCreated())

                .andExpect(jsonPath("$.document_url", is("testFile.pdf")))
                .andExpect(jsonPath("$.file_data", is(testFileData)))
                .andExpect(jsonPath("$.creation_date", is(testFileDate)))
                .andExpect(jsonPath("$.type", is("testType")))
                .andExpect(jsonPath("$.user", is("testUserId")));
    }

    @Test
    @DisplayName("DELETE /document/1 - Success")
    public void deleteDocumentTestSucess() throws Exception {
        byte[] testFileData = {};
        Date testFileDate = new Date();
        Document mockDocument = new Document("testFile.pdf", testFileData, testFileDate, "testType", "testUserId");

        mockDocument.setFile_id(Long.valueOf(1));

        doReturn(Optional.of(mockDocument)).when(service).findById(Long.valueOf(1));
        doReturn(true).when(service).delete(Long.valueOf(1));

        this.mockMvc.perform(delete("/document/{id}", Long.valueOf(1)))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("DELETE /document/1 - Fail")
    public void deleteDocumentTestFail() throws Exception {
        byte[] testFileData = {};
        Date testFileDate = new Date();
        Document mockDocument = new Document("testFile.pdf", testFileData, testFileDate, "testType", "testUserId");

        mockDocument.setFile_id(Long.valueOf(1));

        doReturn(Optional.of(mockDocument)).when(service).findById(Long.valueOf(1));
        doReturn(false).when(service).delete(Long.valueOf(1));

        this.mockMvc.perform(delete("/document/{id}", Long.valueOf(1)))
                .andExpect(status().isInternalServerError());
    }

    static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
